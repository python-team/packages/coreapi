Source: coreapi
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Pierre-Elliott Bécue <becue@crans.org>
Homepage: https://github.com/core-api/python-client
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               python3-all,
               python3-setuptools,
               python3-coreschema,
               python3-itypes,
               python3-legacy-cgi,
               python3-requests,
               python3-uritemplate,
Rules-Requires-Root: no
Standards-Version: 4.1.3
Testsuite: autopkgtest-pkg-python
Vcs-Git: https://salsa.debian.org/python-team/packages/coreapi.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/coreapi

Package: python3-coreapi
Architecture: all
Depends:
 python3-pkg-resources,
 python3-legacy-cgi,
 ${misc:Depends},
 ${python3:Depends},
Description: Python3 client library for Core API
 Core API is a format-independent Document Object Model for representing
 Web APIs.
 .
 It can be used to represent either Schema or Hypermedia responses, and
 allows one to interact with an API at the layer of an application
 interface, rather than a network interface.
 .
 This package provides a Python3 client for such APIs
